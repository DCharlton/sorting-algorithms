#include <iostream>
#include <fstream>
#include <iomanip>
using namespace std;

void quick_sort(int A[], int start, int end);
int partition(int A[], int start, int end);
void swapArr(int A[], int a, int b);

int main() {
  ifstream fin;
  int in;
  int numbers[100];

  fin.open("numbers");
  if (fin.fail()) {
    cout << "No File Found." << endl;
    return 1;
  }

  for (int i = 0; i < 100; i++) {
    fin >> in;
    numbers[i] = in;
  }

  quick_sort(numbers, 0, 99);

  for (int i = 0; i < 100; i++) {
    cout << setw(2) << left << numbers[i];
    if ((i+1)%10 == 0)
      cout << endl;
    else 
      cout << ", ";
  }

  return 0;
}

void quick_sort(int A[], int start, int end) {
  if (start < end) {
    int part = partition(A, start, end);

    quick_sort(A, start, part-1);
    quick_sort(A, part+1, end);
  }
}

int partition(int A[], int start, int end) {
  int high = start, pivot = A[end];

  for (int low = start; low < end; low++) {
    if (A[low] < pivot) {
      swapArr(A, low, high);
      high++;
    }
  }

  swapArr(A, high, end);
  return high;
}

void swapArr(int A[], int a, int b) {
  int temp = A[a];
  A[a] = A[b];
  A[b] = temp; 
}
