#include <iostream>
#include <fstream>
#include <iomanip>
using namespace std;

void merge(int A[ ] , int start, int mid, int end);
void merge_sort(int A[ ] , int start, int end);

int main() {
  ifstream fin;
  int in;
  int numbers[100];

  fin.open("numbers");
  if (fin.fail()) {
    cout << "No File Found." << endl;
    return 1;
  }

  for (int i = 0; i < 100; i++) {
    fin >> in;
    numbers[i] = in;
  }

  merge_sort(numbers, 0, 99);

  for (int i = 0; i < 100; i++) {
    cout << setw(2) << left << numbers[i];
    if ((i+1)%10 == 0)
      cout << endl;
    else 
      cout << ", ";
  }

  return 0;
}

void merge(int A[ ] , int start, int mid, int end) {
  int first = start; int second = mid+1;
  int firstTop = mid, secondTop = end;
  int newArr[end-start+1];

  for (int i = 0; i <= end-start+1; i++) {
    if (first > firstTop) {
      newArr[i] = A[second];
      second++;
    } else if (second > secondTop) {
      newArr[i] = A[first];
      first++;
    } else if (A[first] < A[second]) {
      newArr[i] = A[first];
      first++;
    } else {
      newArr[i] = A[second];
      second++;
    }
  }

  int nAC = 0;
  for (int i = start; i <= end; i++) {
    A[i] = newArr[nAC];
  }

}

void merge_sort(int A[ ] , int start, int end) {

  if (start < end) {
    int mid = (start + end) / 2;

    merge_sort(A, start, mid);
    merge_sort(A, mid+1, end);

    merge(A, start, mid, end);
  }
}
