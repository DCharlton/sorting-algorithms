#include <iostream>
#include <fstream>
#include <iomanip>
using namespace std;

int main() {
  ifstream fin;
  int in;
  int numbers[100];

  fin.open("numbers");
  if (fin.fail()) {
    cout << "No File Found." << endl;
    return 1;
  }

  for (int i = 0; i < 100; i++) {
    fin >> in;
    numbers[i] = in;
  }

  bool swapped = true;
  while (swapped) {
    swapped = false;
    for (int i = 0; i < 99; i++) {
      if (numbers[i] > numbers[i+1]) {
        // cout << "Swapping [" << numbers[i] << "," << numbers[i+1] << "]" << endl;
        int temp = numbers[i+1];
        numbers[i+1] = numbers[i];
        numbers[i] = temp;
        swapped = true;
      }
    }
  }

  for (int i = 0; i < 100; i++) {
    cout << setw(2) << left << numbers[i];
    if ((i+1)%10 == 0)
      cout << endl;
    else 
      cout << ", ";
  }

  return 0;
}
