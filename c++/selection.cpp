#include <iostream>
#include <fstream>
#include <iomanip>
using namespace std;

int main() {
  ifstream fin;
  int in;
  int numbers[100];

  fin.open("numbers");
  if (fin.fail()) {
    cout << "No File Found." << endl;
    return 1;
  }

  for (int i = 0; i < 100; i++) {
    fin >> in;
    numbers[i] = in;
  }

  for (int i = 0; i < 100; i++) {
    int bottom = numbers[i];
    for (int f = i; f < 100; f++) {
      if (numbers[f] < bottom) {
        numbers[i] = numbers[f];
        numbers[f]= bottom;
        bottom = numbers[i];
      }
    }
  }

  for (int i = 0; i < 100; i++) {
    cout << setw(2) << left << numbers[i];
    if ((i+1)%10 == 0)
      cout << endl;
    else 
      cout << ", ";
  }

  return 0;
}