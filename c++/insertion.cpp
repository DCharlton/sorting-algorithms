#include <iostream>
#include <fstream>
#include <iomanip>
using namespace std;

int main() {
  ifstream fin;
  int in;
  int numbers[100];

  fin.open("numbers");
  if (fin.fail()) {
    cout << "No File Found." << endl;
    return 1;
  }

  for (int i = 0; i < 100; i++) {
    fin >> in;
    numbers[i] = in;
  }

  for (int i = 0; i < 100; i++) {
    int key = numbers[i];
    for (int b = i; b > 0; b--) {
      if (numbers[b-1] > key) {
        numbers[b] = numbers[b-1];
        numbers[b-1] = key;
      }
      else {
        break;
      }
    }
  }

  for (int i = 0; i < 100; i++) {
    cout << setw(2) << left << numbers[i];
    if ((i+1)%10 == 0)
      cout << endl;
    else 
      cout << ", ";
  }

  return 0;
}